import java.text.ParseException;
import java.util.Date;

enum WindDirection{
    NORTH,
    EAST,
    SOUTH,
    WEST
}

public class Hour {

    public Hour(Date date, double temperature, int humidity, WindDirection windDirection, double windSpeed) throws ParseException {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windDirection  = windDirection;
        this.windSpeed = windSpeed;
    }

    Date date;

    double temperature;

    int humidity;

    WindDirection windDirection;

    double windSpeed;

    @Override
    public String toString() {
        return "Hour{" +
                "date = " + date +
                ", temperature = " + temperature +
                ", humidity = " + humidity +
                ", windDirection = " + windDirection +
                ", windSpeed = " + windSpeed +
                '}';
    }
}
