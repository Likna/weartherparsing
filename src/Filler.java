import java.io.*;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Filler {

    Hour[] hours;

    Hour[] getHourWeatherArray(File file) throws java.io.IOException, ParseException {

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        List<String> collect = bufferedReader.lines()
                .filter(s -> s.charAt(0) >= 48 && s.charAt(0) <= 57)
                .collect(Collectors.toList());

        hours = new Hour[collect.size()];

        int i = 0;
        for (String str :
                collect) {
            createHour(str, i);
            i++;
        }

        return hours;
    }

    void createHour(String str, int index) throws ParseException {

        Date date = parsedDate(str.split(",")[0]);
        double temperature = Double.parseDouble(str.split(",")[1]);
        int humidity = (int) Double.parseDouble(str.split(",")[2]);
        WindDirection windDirection = determineDirection(str.split(",")[3]);//направление сделать стрококй север, юг и пр
        double windSpeed = Double.parseDouble(str.split(",")[3]);
        hours[index] = new Hour(date,temperature,humidity,windDirection,windSpeed);

    }

    Date parsedDate(String date)throws ParseException {
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyyMMdd'T'HHmm");
        return sdf.parse(date);
    }

    WindDirection determineDirection(String direction){
        double grad = Double.parseDouble(direction);

        if (grad<=45||grad>345)
            return WindDirection.NORTH;
        if (grad>45 && grad<=135)
            return WindDirection.EAST;
        if (grad>135 && grad<=225)
            return WindDirection.SOUTH;
        if (grad>225 && grad<=345)
            return WindDirection.WEST;

        throw new InvalidParameterException();
    }

    void addFile(Collection<String> params) throws IOException {

        File file = new File("WeatherAnalise.txt");
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        for (String s:
             params) {
            bufferedWriter.write(s);
        }
        bufferedWriter.flush();
        bufferedWriter.close();
    }
}
