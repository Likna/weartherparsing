import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public class Weather {

    public Weather(String str) throws IOException, ParseException {
        weatherFile = new File(str);
        filler = new Filler();
        this.hour = filler.getHourWeatherArray(weatherFile);
    }

    final File weatherFile;
    Filler filler;

    Hour[] hour;

    Double getAverageTemp(){
        double sum=0;
        for (int i = 0; i < hour.length; i++) {
            sum+=hour[i].temperature;
        }
        return sum/hour.length;
    }

    Double getAverageHumidity(){
        double sum=0;
        for (int i = 0; i < hour.length; i++) {
            sum+=hour[i].humidity;
        }
        return sum/hour.length;
    }

    Double getAverageSpeed(){

        double sum=0;
        for (int i = 0; i < hour.length; i++) {
            sum+=hour[i].windSpeed;
        }
        return sum/hour.length;
    }

    Date maxTemp(){
        return Arrays.stream(hour).max(new Comparator<Hour>() {
            @Override
            public int compare(Hour o1, Hour o2) {
                if (o1.temperature>o2.temperature) {
                    return 1;
                }
                if (o1.temperature<o2.temperature) {
                    return 1;
                }
                return 0;
            }
        }).get().date;
    }

    Date minHumid(){
        Date date = Arrays.stream(hour).min(new Comparator<Hour>() {
            @Override
            public int compare(Hour o1, Hour o2) {
                if (o1.humidity > o2.humidity) {
                    return 1;
                }
                if (o1.humidity < o2.humidity) {
                    return 1;
                }
                return 0;
            }
        }).get().date;
        return date;
    }

    Date maxWind(){
        Date date = Arrays.stream(hour).max(new Comparator<Hour>() {
            @Override
            public int compare(Hour o1, Hour o2) {
                if (o1.windSpeed > o2.windSpeed) {
                    return 1;
                }
                if (o1.windSpeed < o2.windSpeed) {
                    return 1;
                }
                return 0;
            }
        }).get().date;
        return date;
    }

    WindDirection freqDir(){

        int northTimes = 0;
        int eastTimes = 0;
        int southTimes = 0;
        int westTimes = 0;

        for (Hour value : hour) {
            switch (value.windDirection) {
                case NORTH:
                    northTimes++;
                    break;
                case EAST:
                    eastTimes++;
                    break;
                case SOUTH:
                    southTimes++;
                    break;
                case WEST:
                    westTimes++;
                    break;
            }
        }

        if ((northTimes>=eastTimes) && (northTimes>=southTimes) && (northTimes>=westTimes)) {
            return WindDirection.NORTH;
        }

        if ((eastTimes>=northTimes) && (eastTimes>=southTimes) && (eastTimes>=westTimes)) {
            return WindDirection.EAST;
        }

        if ((southTimes>=northTimes) && (southTimes>=eastTimes) && (southTimes>=westTimes)) {
            return WindDirection.SOUTH;
        }

        return WindDirection.WEST;
    }

    ArrayList<String> result(){
        ArrayList<String> result = new ArrayList<>();
        result.add("Average humidity is " + getAverageHumidity().toString() + "\n");
        result.add("Average wind speed is " + getAverageSpeed().toString() + "\n");
        result.add("Average temperature is " + getAverageTemp().toString() + "\n");
        result.add("The max temp was registered " + maxTemp().toString() + "\n");
        result.add("The max wind speed was registered " + maxWind().toString() + "\n");
        result.add("The min humid was registered " + minHumid().toString() + "\n");
        result.add("The most frequently wind direction is " + freqDir().toString() + "\n");
        return result;
    }

    public static void main(String[] args) throws IOException, ParseException {
        Weather weather = new Weather("D:\\Java (II sem)\\Weather\\dataexport_20210320T064822.csv");
        weather.filler.addFile(weather.result());
    }



}
